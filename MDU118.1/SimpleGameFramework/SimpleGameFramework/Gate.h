#pragma once
#include "GameObject.h"
#include "Player.h"

class Gate :
	public GameObject
{
public:
	Gate();
	virtual ~Gate();

	void Disable(Player* taxPayer);

	virtual GameObjectType GetType()
	{
		return egotGate;
	}

	virtual void SaveSpecificData(std::ofstream& saveFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

	

public:
	int gateCost = 3;
	bool isActive = 1;
};

