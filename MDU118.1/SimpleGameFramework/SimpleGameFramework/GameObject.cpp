#include "stdafx.h"
#include "GameObject.h"
#include "GameFramework.h"
#include "Player.h"
#include "PickUp.h"
#include "LaunchPad.h"
#include "Enemy.h"
#include "Wall.h"
#include "Gate.h"
#include "EndLevel.h"


GameObject::GameObject()
{
}


GameObject::~GameObject()
{
	//tidy children
	for (GameObject* childPtr : Children)
	{
		delete Parent;
		Parent = nullptr;
		delete childPtr;
		childPtr = nullptr;
	}
}

void GameObject::SaveToFile(std::ofstream& saveFile)
{
	//Save Location
	saveFile << GetType() << std::endl;
	saveFile << Location.X << "," << Location.Y << std::endl;
	saveFile << Children.size() << std::endl;

	SaveSpecificData(saveFile);
	saveFile << std::endl;

	//ranged for loop// //equivalent to a for each loop in c#
	for (GameObject* childPtr : Children)
	{
		childPtr->SaveToFile(saveFile);
	}

}

void GameObject::SaveSpecificData(std::ofstream & saveFile)
{
}

void GameObject::LoadFromFile(std::ifstream & loadFile)
{
	// load our location
	char dummyChar;
	loadFile >> Location.X >> dummyChar >> Location.Y;
	size_t numChildren = 0;
	loadFile >> numChildren;

	LoadSpecificData(loadFile);
	//loadFile >> dummyChar;

	// load in all of the children
	for (size_t count = 0; count < numChildren; ++count)
	{
		// read in the type of object
		int objectType = 0;
		loadFile >> objectType;

		GameObject* childPtr = nullptr;
		switch (objectType)
		{
		case egotBaseGameObject:
			childPtr = new GameObject();
			break;
		case egotPlayer:
			childPtr = new Player();
			playerExists = true;
			playerPtr = childPtr;
			break;
		case egotEnemy:
			childPtr = new Enemy();
			break;
		case egotLaunchPad:
			childPtr = new LaunchPad();
			break;
		case egotPickUp:
			childPtr = new PickUp();
			break;
		case egotGate:
			childPtr = new Gate();
			break;
		case egotWall:
			childPtr = new Wall();
			break;
		case egotEndLevel:
			childPtr = new EndLevel();
			break;
		}

		childPtr->LoadFromFile(loadFile);

		Children.push_back(childPtr);
		childPtr->Parent = this;

		/*
		Above is the equivalent of this
		if (objectType == egotBaseGameObject)
		{}
		else if (objectType == egotPlayer)
		{}
		*/
	}
}

void GameObject::LoadSpecificData(std::ifstream & loadFile)
{
}

void GameObject::Update(double deltaTime)
{
}

void GameObject::MovePlayer(std::string& direction)
{
	if (direction == "Restart")
	{
		for (GameObject* childPtr : Children)
		{
			if (childPtr->GetType() != egotPlayer)
			{
					//childPtr->Location = Vector::initialLocation;
			}
		}
	}
	else
	{
		for (GameObject* childPtr : Children)
		{
			if (childPtr->GetType() != egotPlayer)
			{
				if (direction == "up")
					childPtr->Location.Y += TileSize;
				if (direction == "down")
					childPtr->Location.Y -= TileSize;
				if (direction == "right")
					childPtr->Location.X -= TileSize;
				if (direction == "left")
					childPtr->Location.X += TileSize;
			}
		}
	}
}

void GameObject::Render(Gdiplus::Graphics & canvas)
{
	// Save the current transformation of the scene
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// offset our axes to our location
	canvas.TranslateTransform((Gdiplus::REAL)Location.X,
							(Gdiplus::REAL)Location.Y);

	///////////////////////////////////////Drawing code below here

	
	// draw all of the children
	for (GameObject* childPtr : Children)
	
		childPtr->Render(canvas);


		//draw at 0 
		switch (GetType())
		{
		case egotBaseGameObject:
			//GameFrameworkInstance.DrawCircle(canvas, Vector2i::Zero, 50, true, Gdiplus::Color::Magenta);
			break;
		case egotPlayer:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, Gdiplus::Color::WhiteSmoke);
			break;
		case egotEnemy:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, RenderColour());
			break;
		case egotLaunchPad:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, RenderColour());
			break;
		case egotPickUp:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, RenderColour());
			break;
		case egotGate:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, Gdiplus::Color::Orange);
			break;
		case egotWall:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, Gdiplus::Color::SlateGray);
			break;
		case egotEndLevel:
			GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i::Zero, Vector2i(25, 25)), true, Gdiplus::Color::BlueViolet);
			break;
		}
	

	///////////////////////////////////////Drawing code above here

	//Restore previous transform
	canvas.SetTransform(&transform);
}

void GameObject::SwitchType()
{
}

void GameObject::FoundObject(int objectType, Vector2i placementPos)
{
	GameObject* foundObject = nullptr;
	for (GameObject* childPtr : Children)
	{
		GameObject& childRef = *childPtr;

		// convert the child's location to world space
		Vector2i testLocation = childRef.Location;
		testLocation += Location;

		if (placementPos == testLocation)
		{
			foundObject = childPtr;
		}
	}

	if (foundObject)
	{
		foundObject->SwitchType();
	}
}

void GameObject::AddObject(int objectType, Vector2i spawnLocation)
{
	//create a new game object
	GameObject* newObject = nullptr;
	switch (objectType)
	{
	case egotBaseGameObject:
		newObject = new GameObject();
		break;
	case egotPlayer:
		newObject = new Player();
		break;
	case egotGate:
		newObject = new Gate();
		break;
	case egotWall:
		newObject = new Wall();
		break;
	case egotEnemy:
		newObject = new Enemy();
		break;
	case egotPickUp:
		newObject = new PickUp();
		break;
	case egotLaunchPad:
		newObject = new LaunchPad();
		break;
	case egotEndLevel:
		newObject = new EndLevel();
		break;
	}

	newObject->Location = spawnLocation;
	newObject->Location -= Location;

	// add the object to the level
	Children.push_back(newObject);
	newObject->Parent = this;
}

void GameObject::Placement(int objectType, Vector2i placementPos)
{
	GameObject* foundObject = nullptr;
	for (GameObject* childPtr : Children)
	{
		GameObject& childRef = *childPtr;

		// convert the child's location to world space
		Vector2i testLocation = childRef.Location;
		testLocation += Location;

		if (placementPos == testLocation)
		{
			foundObject = childPtr;
		}
	}

	if (foundObject)
	{
		if (foundObject->GetType() == egotPlayer)
		{
			playerExists = false;
		}
		// remove the selected object
		Children.remove(foundObject);
		delete foundObject;

		// exit the function at this point
		return;
	}

	if ((objectType == egotPlayer) && (playerExists == false))
	{
		playerExists = true;
		AddObject(objectType, placementPos);
	}
	else if ((objectType == egotPlayer) && playerExists)
	{
		//do Nothing;
	}
	else
		AddObject(objectType, placementPos);

	
}

