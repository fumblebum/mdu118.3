#pragma once

#include "GameManager.h"
#include "GameObject.h"
#include "Player.h"

enum EnemyType
{
	egotRed,
	egotBlue
};

class Enemy : public GameObject
{
public:
	Enemy();
	virtual ~Enemy();

	void Collision(Player* target);
	
	void Attack();

	void Disable();

	EnemyType GetEnemyType()
	{
		switch (enemyType)
		{
		case 0:
			return egotRed;
			break;
		case 1:
			return egotBlue;
			break;
		}
		return egotRed;
	}

	virtual GameObjectType GetType()
	{
		return egotEnemy;
	}

	virtual Gdiplus::Color RenderColour()
	{
		if (enemyType == 0)
			return Gdiplus::Color::Red;
		else if (enemyType == 1)
			return Gdiplus::Color::Blue;
		else
			return Gdiplus::Color::Magenta;
	}

	virtual void SaveSpecificData(std::ofstream& saveFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

	virtual void SwitchType();

public:
	bool enemyType = 0;
	int damage = 10;
	bool isActive = 1;

};

