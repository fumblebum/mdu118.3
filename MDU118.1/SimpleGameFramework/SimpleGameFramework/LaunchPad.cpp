#include "stdafx.h"
#include "LaunchPad.h"


LaunchPad::LaunchPad()
{
}


LaunchPad::~LaunchPad()
{
}

void LaunchPad::BoostPlayer(Player* target)
{
}

void LaunchPad::SaveSpecificData(std::ofstream & saveFile)
{
	saveFile << this->LaunchType() << std::endl;
	saveFile << this->launchPower << std::endl;
}

void LaunchPad::LoadSpecificData(std::ifstream & loadFile)
{
	loadFile >> launchType;
	loadFile >> launchPower;
}

void LaunchPad::SwitchType()
{
	if (launchType == 1)
		launchType = 0;
	else if (launchType == 0)
		launchType = 1;
}
