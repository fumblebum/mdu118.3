#pragma once
#include "GameObject.h"

enum PickUpType
{
	egotPickUpHP,
	egotPickUpSp
};

class PickUp :
	public GameObject
{
public:
	PickUp();
	virtual ~PickUp();

		PickUpType GetPickUpType()
	{
			switch (pickUpType)
			{
			case 0:
				return egotPickUpHP;
				break;
			case 1:
				return egotPickUpSp;
				break;
			}
			return egotPickUpHP;
	}

	virtual GameObjectType GetType()
	{
		return egotPickUp;
	}

	

	virtual void SaveSpecificData(std::ofstream& saveFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

	virtual void SwitchType();
	virtual Gdiplus::Color RenderColour()
	{
		if (pickUpType == 0)
			return Gdiplus::Color::Green;
		else if (pickUpType == 1)
			return Gdiplus::Color::Aqua;
		else
			return Gdiplus::Color::Magenta;
	}

public: 
	int statIncrement = 5;
	bool pickUpType = 0;
};

