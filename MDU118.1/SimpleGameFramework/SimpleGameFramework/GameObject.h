#pragma once

#include <list>
#include "GameFramework.h"

#include <iostream>
#include <fstream>

//used to set types, e(for enum) followed by acronym, eg egot = enum game object type
enum GameObjectType
{
	egotBaseGameObject,
	egotPlayer,
	egotEnemy,
	egotLaunchPad,
	egotPickUp,
	egotGate,
	egotWall,
	egotEndLevel
};

class GameObject
{
public:
	GameObject();
	~GameObject();

	void SaveToFile(std::ofstream& saveFile);
	virtual void SaveSpecificData(std::ofstream& saveFile);

	void LoadFromFile(std::ifstream& loadFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

	void Update(double deltaTime);
	void Render(Gdiplus::Graphics & canvas);
	
	virtual Gdiplus::Color RenderColour()
	{
		return Gdiplus::Color::Magenta;
	}

	virtual void SwitchType();

	virtual GameObjectType GetType()
	{
		return egotBaseGameObject;
	}

	void FoundObject(int objectType, Vector2i placementPos);
	void AddObject(int objectType, Vector2i spawnLocation);
	void Placement(int objectType, Vector2i spawnLoaction);
	void MovePlayer(std::string& direction);

public:


	Vector2i Location;
	Vector2i initialLocation;
	GameObject* Parent = nullptr;	
	std::list<GameObject*> Children;

	GameObject* playerPtr = nullptr;
	bool playerExists = 0;
};

