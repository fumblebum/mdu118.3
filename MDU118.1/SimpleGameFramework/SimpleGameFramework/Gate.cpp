#include "stdafx.h"
#include "Gate.h"


Gate::Gate()
{
}


Gate::~Gate()
{
}

void Gate::Disable(Player * taxPayer)
{
}

void Gate::SaveSpecificData(std::ofstream & saveFile)
{
	saveFile << this->gateCost << std::endl;
	saveFile << this->isActive << std::endl;
}

void Gate::LoadSpecificData(std::ifstream & loadFile)
{
	loadFile >> gateCost;
	loadFile >> isActive;
}
