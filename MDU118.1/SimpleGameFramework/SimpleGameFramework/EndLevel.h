#pragma once
#include "GameObject.h"
class EndLevel :
	public GameObject
{
public:
	EndLevel();
	virtual ~EndLevel();

	virtual GameObjectType GetType()
	{
		return egotEndLevel;
	}
};

