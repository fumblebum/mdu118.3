#include "stdafx.h"
#include "PickUp.h"


PickUp::PickUp()
{
}


PickUp::~PickUp()
{
}

void PickUp::SwitchType()
{
	if (pickUpType == 0)
		pickUpType = 1;
	else if (pickUpType == 1)
		pickUpType = 0;
}

void PickUp::SaveSpecificData(std::ofstream & saveFile)
{
	saveFile << this->GetPickUpType() << std::endl;
	saveFile << this->statIncrement << std::endl;
}

void PickUp::LoadSpecificData(std::ifstream & loadFile)
{
	loadFile >> pickUpType;
	loadFile >> statIncrement;
}
