#include "stdafx.h"
#include "GameManager.h"
#include "Player.h"
#include "PickUp.h"
#include "LaunchPad.h"
#include "Enemy.h"
#include "Wall.h"
#include "Gate.h"
#include "EndLevel.h"

GameManager& GameManager::Instance()
{
	static GameManager instance;

	return instance;
}

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::BeginPlay()
{
	//instantiate top level object
	levelObjectPtr = new GameObject();
	levelObjectPtr->Location = Vector2i(0, 0);
	
	
}

void GameManager::EndPlay()
{

	delete levelObjectPtr;
	levelObjectPtr = nullptr;
}

void GameManager::Update(double deltaTime)
{
	
}

void GameManager::Render(Gdiplus::Graphics& canvas, const CRect& clientRect)
{
	

	// Save the current transformation of the scene
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	//draw all the objects
	levelObjectPtr->Render(canvas);


	canvas.ScaleTransform(0.5f, 0.5f);
	canvas.RotateTransform(30.0f);
	canvas.TranslateTransform(200.0f, 200.0f);
	
	canvas.SetTransform(&transform);

	if (showControls == 1)
	{
		GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(22 * TileSize, 22 * TileSize)), true, Gdiplus::Color::WhiteSmoke);

		GameFrameworkInstance.DrawText(canvas, Vector2i::Zero, 10, "Arial", "Q: Toggle Controls Display \n[F1]Save File 1 | [z]Load File 1 \n[F2]Save File 2 | [x]Load File 2 \n[F3]Save File 3 | [c]Load File 3", Gdiplus::Color::Black);
		GameFrameworkInstance.DrawText(canvas, Vector2i(0, 6 * TileSize), 10, "Arial", "Left Click: Place/Remove Object \nRight Click: Change Type \nArrow Keys: Move Level \nR: Original Location", Gdiplus::Color::Black);
		GameFrameworkInstance.DrawText(canvas, Vector2i(0, 12 * TileSize), 10, "Arial", "1: Player \n2: Wall \n3: Enemy \n4: Gate \n5: PickUp \n6: LaunchPad \n7: EndLevel", Gdiplus::Color::Black);
	}
	else
		GameFrameworkInstance.DrawText(canvas, Vector2i::Zero, 10, "Arial", "Q: Toggle Controls Display", Gdiplus::Color::SlateGray);
	
	{
		if (!gameMode)
		{
			GameFrameworkInstance.DrawText(canvas, Vector2i(10 * TileSize, 15 * TileSize), 20, "Arial", "EDITING", Gdiplus::Color::SlateGray);
		}
		else if (gameMode)
		{
			GameFrameworkInstance.DrawText(canvas, Vector2i(10 * TileSize, 15 * TileSize), 20, "Arial", "PLAYING", Gdiplus::Color::Magenta);
		}
	}
	// Restore the transformation of the scene
	canvas.SetTransform(&transform);
}

//Saving
void GameManager::SaveFile(int fileNumber)
{
	switch (fileNumber)
	{
		case 1:
			{
				////////////////Create File For Saving////////////////////////////// 
				std::ofstream saveFile("LevelFile1.csv");

				//save version number on first line
				saveFile << 3 << std::endl;

				//save out level object pointers
				levelObjectPtr->SaveToFile(saveFile);

				saveFile.close();
				break;
			}
		case 2:
		{
			////////////////Create File For Saving////////////////////////////// 
			std::ofstream saveFile("LevelFile2.csv");

			//save version number on first line
			saveFile << 3 << std::endl;

			//save out level object pointers
			levelObjectPtr->SaveToFile(saveFile);

			saveFile.close();
			break;
		}
		case 3:
		{
			////////////////Create File For Saving////////////////////////////// 
			std::ofstream saveFile("LevelFile3.csv");

			//save version number on first line
			saveFile << 3 << std::endl;

			//save out level object pointers
			levelObjectPtr->SaveToFile(saveFile);

			saveFile.close();
			break;
		}
	}

}

//Load File
void GameManager::LoadFile(int fileNumber)
{
	switch (fileNumber)
	{
		case 1:
		{

			std::ifstream levelFile("LevelFile1.csv");

			int version = 0;
			levelFile >> version;

			// is the file valid?
			if (version == 3)
			{
				delete levelObjectPtr;
				levelObjectPtr = nullptr;

				int objectType = 0;
				levelFile >> objectType;

				levelObjectPtr = new GameObject();
				levelObjectPtr->LoadFromFile(levelFile);
			}

			levelFile.close();
			break;
		}
		case 2:
		{

			std::ifstream levelFile("LevelFile2.csv");

			int version = 0;
			levelFile >> version;

			// is the file valid?
			if (version == 3)
			{
				delete levelObjectPtr;
				levelObjectPtr = nullptr;

				int objectType = 0;
				levelFile >> objectType;

				levelObjectPtr = new GameObject();
				levelObjectPtr->LoadFromFile(levelFile);
			}

			levelFile.close();
			break;
		}
		case 3:
		{

			std::ifstream levelFile("LevelFile3.csv");

			int version = 0;
			levelFile >> version;

			// is the file valid?
			if (version == 3)
			{
				delete levelObjectPtr;
				levelObjectPtr = nullptr;

				int objectType = 0;
				levelFile >> objectType;

				levelObjectPtr = new GameObject();
				levelObjectPtr->LoadFromFile(levelFile);
			}

			levelFile.close();
			break;
		}
	}
	
}

//Toggle Control Display
void GameManager::DisplayControls()
{
	if (showControls == 0)
		showControls = 1;
	else if (showControls == 1)
		showControls = 0;
}

//Switch between editing and playing
void GameManager::SwitchMode()
{
	if (gameMode == 0)
	{
		gameMode = 1;
	}
	else if (gameMode == 1) 
	{
		gameMode = 0;
	}
}

//Check Location and Place/Delete Object
void GameManager::LeftButtonDown(const Vector2i& location, int objectType)
{
	if (gameMode == 0)
	{
		// convert to grid coordinates
		int gridX = location.X / TileSize;
		int gridY = location.Y / TileSize;

		Vector2i spawnLocation(gridX * TileSize, gridY * TileSize);

		//Check Location / Add Object
		levelObjectPtr->Placement(objectType, spawnLocation);
	}
}

//Change Object Type
void GameManager::RightButtonDown(const Vector2i & location, int objectType)
{
	if (gameMode == 0)
	{
		//convert to grid coordinates
		int gridX = location.X / TileSize;
		int gridY = location.Y / TileSize;

		Vector2i spawnLocation(gridX * TileSize, gridY * TileSize);

		//changeType
		levelObjectPtr->FoundObject(objectType, spawnLocation);
	}
}

//Move The Level
void GameManager::UpArrow()
{
	if (gameMode == 0)
	{
		levelObjectPtr->Location.Y += TileSize;
	}
	else if (gameMode == 1)
	{
		playerDirection = "up";
		levelObjectPtr->MovePlayer(playerDirection);
	}
}

void GameManager::DownArrow()
{
	if (gameMode == 0)
	{
		levelObjectPtr->Location.Y -= TileSize;
	}
	else if (gameMode == 1)
	{
		playerDirection = "down";
		levelObjectPtr->MovePlayer(playerDirection);
	}
}

void GameManager::LeftArrow()
{
	if (gameMode == 0)
	{
		levelObjectPtr->Location.X += TileSize;
	}
	else if (gameMode == 1)
	{
		playerDirection = "left";
		levelObjectPtr->MovePlayer(playerDirection);
	}
}

void GameManager::RightArrow()
{
	if (gameMode == 0)
	{
		levelObjectPtr->Location.X -= TileSize;
	}
	else if (gameMode == 1)
	{
		playerDirection = "right";
		levelObjectPtr->MovePlayer(playerDirection);
	}
}

void GameManager::OnR()
{
	if (gameMode == 0)
	{
		levelObjectPtr->Location = Vector2i(0, 0);
	}
	else if (gameMode == 1)
	{
		//levelObjectPtr->loop locations to 0
	}
}


