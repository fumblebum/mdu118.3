#pragma once
#include "GameObject.h"
#include "Player.h"

enum LaunchDir
{
	egotRight,
	egotLeft
};

class LaunchPad : public GameObject
{

public:
	LaunchPad();
	virtual ~LaunchPad();

	void BoostPlayer(Player* target);

	LaunchDir LaunchType()
	{
		switch (launchType)
		{
		case 0:
			return egotRight;
			break;
		case 1:
			return egotLeft;
			break;
		}
		return egotRight;
	}

	virtual GameObjectType GetType()
	{
		return egotLaunchPad;
	}

	virtual void SaveSpecificData(std::ofstream& saveFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

	virtual void SwitchType();

	virtual Gdiplus::Color RenderColour()
	{
		if (launchType == 0)
			return Gdiplus::Color::Purple;
		else if (launchType == 1)
			return Gdiplus::Color::Pink;
		else
			return Gdiplus::Color::Magenta;
	}

public:
	bool launchType = 0;
	int launchPower = 10;
};

