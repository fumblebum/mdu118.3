#pragma once
#include "GameObject.h"
#include "PickUp.h"

class Player : public GameObject
{
public:
	Player();
	virtual ~Player();

	//Runs When Player collides with GameObject
	void Collision(GameObjectType);

	//Runs When Player collides with PickUp
	void PickUp(PickUpType);


	//Positions player at initialLocation or switches to edit mode
	void Respawn();

	//Returns to edit mode
	void EndOfLevel(Vector2i Location);

	virtual GameObjectType GetType()
	{
		return egotPlayer;
	}

	//void Render(Gdiplus::Graphics & canvas);

	virtual void SaveSpecificData(std::ofstream& saveFile);
	virtual void LoadSpecificData(std::ifstream& loadFile);

public:
	int HP = 100;
	int speed = 10;
	int jumpHeight = 10;
	int lives = 3;
	int enemiesDisabled = 0;
	bool direction = 0;
	Vector2i initialLocation;

};



