#include "stdafx.h"
#include "Player.h"


Player::Player()
{
	playerPtr = this;
}


Player::~Player()
{
	delete playerPtr;
	playerPtr = nullptr;
}

void Player::Collision(GameObjectType)
{
}

void Player::PickUp(PickUpType)
{
}



void Player::Respawn()
{
}


void Player::EndOfLevel(Vector2i Location)
{
}


void Player::SaveSpecificData(std::ofstream & saveFile)
{
	saveFile << this->HP << std::endl;
	saveFile << this->speed << std::endl;
	saveFile << this->jumpHeight << std::endl;
	saveFile << this->lives << std::endl;
	saveFile << this->enemiesDisabled << std::endl;
	saveFile << this->initialLocation.X << "," << this->initialLocation.Y << std::endl;

	
}

void Player::LoadSpecificData(std::ifstream & loadFile)
{ 
	// load our location
	char dummyChar;
	loadFile >> HP;
	loadFile >> speed;
	loadFile >> jumpHeight;
	loadFile >> lives;
	loadFile >> enemiesDisabled;
	loadFile >> initialLocation.X >> dummyChar >> initialLocation.Y;
}
